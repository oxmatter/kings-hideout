﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    [SerializeField] private PickUpType _pickUpType;

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.TryGetComponent(out PlayerScript player))
        {
            player.PickUp(_pickUpType);
            Destroy(gameObject);
        }
    }
}

public enum PickUpType
{
    Health
}
