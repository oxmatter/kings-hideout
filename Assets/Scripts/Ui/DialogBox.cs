﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class DialogBox : MonoBehaviour
{
    [SerializeField] private GameObject _dialogBox;
    [SerializeField] private TextMeshProUGUI _dialogTextBox;
    [SerializeField] private List<Speaker> _speakers = new List<Speaker>();
    [SerializeField] private AudioSource speechAudSrc;
    public static DialogBox Instance;
    private List<Coroutine> textRoutines = new List<Coroutine>();

    private void Awake()
    {
        Instance = this;
    }

    public void ShowDialog(string text, SpeakerType speakerType = default, float duration = 5, float timeToRead = 2, bool waitForKey = false, Action endCallback = null)
    {
        CheckAndClearRt();
        textRoutines.Add(StartCoroutine(ShowText(text, speakerType, duration, timeToRead, waitForKey, endCallback)));
    }

    public void ShowDialogArray(string[] strings, SpeakerType speakerType = default,  float duration = 2, Action endCallback = null)
    {
        CheckAndClearRt();
        textRoutines.Add(StartCoroutine(ShowTextArray(strings, speakerType, duration, endCallback)));
    }

    private void CheckAndClearRt()
    {
        if (textRoutines.Count > 0)
        {
            textRoutines.ForEach(StopCoroutine);
        }
    }

    private IEnumerator ShowTextArray(string[] strings, SpeakerType speakerType = default, float duration = 2, Action endCallback = null)
    {
        foreach (string dialogSection in strings)
        {
            yield return ShowText(dialogSection, speakerType, duration, 2, true);
        }
        endCallback?.Invoke();
    }

    private IEnumerator ShowText(string text, SpeakerType speakerType = default, float duration = 5, float timeToRead = 2, bool waitForKey = false, Action endCallback = null)
    {
        _dialogBox.SetActive(true);
        yield return null;
        float durationPerChar = duration / text.Length;
        Speaker speaker = GetSpeaker(speakerType);
        string finalString = speaker != null ? speaker.speakerName + ": " : "";
        foreach (char c in text)
        {
            finalString += c;
            _dialogTextBox.text = finalString;
            PlayRandomSpeech(speaker);
            yield return new WaitForSeconds(durationPerChar);
        }
        yield return new WaitForSeconds(0.25f);
        if (waitForKey)
        {
            _dialogTextBox.text = finalString + "\n(space to continue)";
            yield return WaitForKey(KeyCode.Space);
        }
        else
        {
            yield return new WaitForSeconds(timeToRead);
        }
        _dialogTextBox.text = "";
        _dialogBox.SetActive(false);
        endCallback?.Invoke();
    }

    private void PlayRandomSpeech(Speaker speaker)
    {
        if (speaker == null) return;
        speechAudSrc.Stop();
        speechAudSrc.pitch = Random.Range(speaker.speakerPitch.x, speaker.speakerPitch.y);
        AudioClip spClip = speaker.speechParts[Random.Range(0, speaker.speechParts.Length)];
        speechAudSrc.clip = spClip;
        speechAudSrc.Play();
    }

    private IEnumerator WaitForKey(KeyCode keyCode)
    {
        while (!Input.GetKeyDown(keyCode))
        {
            yield return null;
        }
    }

    private Speaker GetSpeaker(SpeakerType speakerType)
    {
       return _speakers.FirstOrDefault(s => s.speakerType == speakerType);
    }
    
    [Serializable]
    public class Speaker
    {
        public SpeakerType speakerType;
        public string speakerName;
        public Vector2 speakerPitch;
        public AudioClip[] speechParts;
    }
    public enum SpeakerType
    {
        King
    }
}
