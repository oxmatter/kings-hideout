﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHp : MonoBehaviour
{
    [SerializeField] private Slider hpSlider;
    [SerializeField] private PlayerScript _player;

    private void Start()
    {
        UpdateSliderValue(_player.Health.CurrentHp, _player.Health.MaxHp);
        _player.Health.OnTookDamage += (amount) =>
        {
            UpdateSliderValue(_player.Health.CurrentHp, _player.Health.MaxHp);
        };
        _player.Health.OnHpRestored += (amount) =>
        {
            UpdateSliderValue(_player.Health.CurrentHp, _player.Health.MaxHp);
        };
    }

    private void UpdateSliderValue(float current, float max)
    {
        hpSlider.value = current / max;
    }
}
