﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WaveAnounce : MonoBehaviour
{
    [SerializeField] private GameObject waveAnnouncePanel;
    [SerializeField] private TextMeshProUGUI waveText;
    [SerializeField] private EnemySpawner _enemySpawner;

    private void Start()
    {
        _enemySpawner.OnNextWave += (waveNum) => { StartCoroutine(ShowWaveNum(waveNum)); };
        _enemySpawner.OnWaveCompleted += () => { StartCoroutine(ShowWaveCompleted()); };
    }

    private IEnumerator ShowWaveNum(int waveNum)
    {
        waveText.text = "Wave: " + waveNum;
        waveAnnouncePanel.SetActive(true);
        yield return new WaitForSeconds(2f);
        waveAnnouncePanel.SetActive(false);
    }

    private IEnumerator ShowWaveCompleted()
    {
        waveText.text = "Wave Completed!";
        waveAnnouncePanel.SetActive(true);
        yield return new WaitForSeconds(2f);
        waveAnnouncePanel.SetActive(false);
    }
}
