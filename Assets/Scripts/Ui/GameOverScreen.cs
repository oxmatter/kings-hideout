﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverScreen : MonoBehaviour
{
    [SerializeField] private GameObject _gameOverScreen;

    private void Start()
    {
        _gameOverScreen.SetActive(false);
        GameManager.Instance.OnGameOver += () =>
        {
            _gameOverScreen.SetActive(true);
        };
    }
}
