﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class ScreenShake : MonoBehaviour
{
    public static ScreenShake Instance;
    private Vector3 origPos;
    private Transform _transform;
    private float _duration;
    private float _magnitude;
    private Vector3 vl;

    private void Awake()
    {
        Instance = this;
        _transform = transform;
        origPos = _transform.localPosition;
    }

    public void Shake(float duration, float magnitude)
    {
        _duration = duration;
        _magnitude = magnitude;
    }

    private void Update()
    {
        if (_duration > 0)
        {
            Vector3 newPos = _transform.localPosition;
            newPos = Vector3.SmoothDamp(newPos, origPos + Random.insideUnitSphere * _magnitude, ref vl, 0.045f);
            _transform.localPosition = newPos;
            _duration -= Time.deltaTime;
        }
        else
        {
            _transform.localPosition = origPos;
        }
    }
}
