﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health
{
    public float MaxHp { private set; get; }
    public float CurrentHp { private set; get; }
    
    public Action<float> OnTookDamage = delegate { };
    public Action OnHealthDepleted = delegate {  };
    public Action<float> OnHpRestored = delegate { };

    private bool _canTakeDamage;
    public Health(float maxHp)
    {
        MaxHp = maxHp;
        CurrentHp = maxHp;
        _canTakeDamage = true;
    }

    public void TakeDamage(float amount)
    {
        if (!_canTakeDamage) return;
        float hpToRemove = Mathf.Min(CurrentHp, amount);
        CurrentHp -= hpToRemove;
        OnTookDamage(hpToRemove);
        if (CurrentHp <= 0)
        {
            OnHealthDepleted();
            _canTakeDamage = false;
        }
    }

    public void RestoreHp(float amount = 0)
    {
        if (amount == 0) amount = MaxHp;
        float hpToRestore = Mathf.Min(amount, (MaxHp - CurrentHp));
        CurrentHp += hpToRestore;
        _canTakeDamage = true;
        OnHpRestored(hpToRestore);
    }
}
