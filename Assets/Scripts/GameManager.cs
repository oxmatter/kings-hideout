﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private Transform cameraTransform;

    public Transform CameraTransform => cameraTransform;
    [SerializeField] private Transform playerTransform;

    public Transform PlayerTransform => playerTransform;
    public Action OnGameOver = delegate {  };

    public static GameManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        playerTransform.GetComponent<PlayerScript>().Health.OnHealthDepleted += GameOver;
    }

    private void GameOver()
    {
        playerTransform.GetComponent<MouseLook>().LockCursor(false);
        OnGameOver.Invoke();
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
