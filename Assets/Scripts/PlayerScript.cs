﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour, IDamagable
{
    [SerializeField] private float _moveSpeed;
    [SerializeField] private Sword _shord;
    [SerializeField] private CharacterController _characterController;
    [SerializeField] private Transform _cameraHolder;
    [SerializeField] private MouseLook _mouseLook;
    private Transform _transform;
    private Health _health;
    public Health Health => _health;
    private bool canControl = true;

    private void Awake()
    {
        _health = new Health(100);
        _health.OnTookDamage += TookDamage;
        _health.OnHealthDepleted += HealthDepleted;
        _health.OnHpRestored += HealthRestored;
        
        _transform = transform;
    }

    private void Start()
    {
        _mouseLook.Init(_transform, _cameraHolder.transform);
    }

    private void OnDestroy()
    {
        _health.OnTookDamage -= TookDamage;
        _health.OnHealthDepleted -= HealthDepleted;
        _health.OnHpRestored -= HealthRestored;
    }

    private void Update()
    {
        if (!canControl) return;

        _mouseLook.MoveLook(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");
        if (Input.GetButtonDown("Fire1")) _shord.Attack();

        Vector3 moveDir = _transform.right * x + _transform.forward * z;
        _characterController.Move(moveDir * (_moveSpeed * Time.deltaTime));
    }

    public void PickUp(PickUpType pickUpType)
    {
        switch (pickUpType)
        {
            case PickUpType.Health:
                Health.RestoreHp(10f);
                break;
        }
    }

    public void Damage(float amount)
    {
        _health.TakeDamage(amount);
    }

    private void TookDamage(float amount)
    {
        ScreenShake.Instance.Shake(0.15f, 0.5f);
    }

    private void HealthDepleted()
    {
        canControl = false;
    }

    private void HealthRestored(float amount)
    {
        
    }
}
