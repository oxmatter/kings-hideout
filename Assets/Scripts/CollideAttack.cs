﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollideAttack : MonoBehaviour
{
    private float _damageCooldown = 1f;
    private float _damageAmount = 10f;
    private bool _attackStarted;
    private float _attackTimeCounter;
    public Action OnAttack = delegate {  };

    public void Init(float damageCooldown, float damageAmount)
    {
        _damageCooldown = damageCooldown;
        _damageAmount = damageAmount;
    }

    private void Update()
    {
        _attackTimeCounter -= Time.deltaTime;
        if (_attackTimeCounter <= 0)
        {
            _attackStarted = false;
        }
    }

    private void OnCollisionStay(Collision other)
    {
        if (_attackStarted) return;
        if (other.gameObject.TryGetComponent(out IDamagable damagable))
        {
            if (damagable is PlayerScript)
            {
                OnAttack();
                _attackStarted = true;
                _attackTimeCounter = _damageCooldown;
                damagable.Damage(_damageAmount);
            }
        }
    }
}
