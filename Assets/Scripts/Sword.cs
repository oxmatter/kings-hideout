﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : MonoBehaviour
{
    [SerializeField] private Transform attackStartTransform;
    [SerializeField] private float attackTime = 0.2f;
    [SerializeField] private float attackDamage = 5f;
    [SerializeField] private float attackDistance = 2f;
    [SerializeField] private Animator _animator;
    [SerializeField] private Transform hitFx;
    private bool attacking;
    private static readonly int AttackAnimTrigger = Animator.StringToHash("Attack");
    private Coroutine attackRoutine;
    public void Attack()
    {
        if (attacking) return;
        if (attackRoutine != null)
        {
            StopCoroutine(attackRoutine);
        }

        attackRoutine = StartCoroutine(AttackCr());
    }

    // private void Update()
    // {
    //     CastAttackRay();
    // }

    IEnumerator AttackCr()
    {
        attacking = true;
        _animator.SetTrigger(AttackAnimTrigger);
        CastAttackRay();
        yield return new WaitForSeconds(attackTime);
        attacking = false;
    }

    private void CastAttackRay()
    {
        RaycastHit hit;
        Vector3 rayDir = attackStartTransform.TransformDirection(Vector3.forward);
        if (Physics.Raycast(attackStartTransform.position, rayDir, out hit, attackDistance))
        {
            TryDamage(hit);
            Debug.DrawRay(attackStartTransform.position, rayDir * attackDistance, Color.green);
        }
        else
        {
            Debug.DrawRay(attackStartTransform.position, rayDir * attackDistance, Color.red);
        }
    }

    private void TryDamage(RaycastHit hit)
    {
        if (hit.transform.TryGetComponent(out IDamagable damagable))
        {
            if (damagable is PlayerScript)
            {
            }
            else
            {
                ScreenShake.Instance.Shake(0.1f, 0.3f);
                Instantiate(hitFx, hit.point, Quaternion.identity);
                damagable.Damage(attackDamage);
            }
        }
    }
}
