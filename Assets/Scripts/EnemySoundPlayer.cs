﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemySoundPlayer : MonoBehaviour
{
    [SerializeField] private Vector2 pitchRange;
    [SerializeField] private AudioClip[] idleSounds;
    [SerializeField] private AudioClip[] hitSounds;
    [SerializeField] private AudioClip[] dieSounds;
    [SerializeField] private AudioClip[] attackSounds;
    [SerializeField] private Enemy _enemy;
    [SerializeField] private CollideAttack _collideAttack;

    private void Start()
    {
        _enemy.OnKilled += () =>
        {
            CreateSound(dieSounds[Random.Range(0, dieSounds.Length)]);
        };
        _enemy.OnTookDamage += () =>
        {
            CreateSound(hitSounds[Random.Range(0, hitSounds.Length)]);
        };
        _collideAttack.OnAttack += () =>
        {
            CreateSound(attackSounds[Random.Range(0, attackSounds.Length)]);
        };
        StartIdleSounds();
    }

    private void StartIdleSounds()
    {
        StartCoroutine(PlayIdleSounds());
    }

    IEnumerator PlayIdleSounds()
    {
        while (_enemy.IsAlive)
        {
            AudioClip idleSound = CreateSound(idleSounds[Random.Range(0, idleSounds.Length)]);
            float delay = Random.Range(2f, 5f);
            yield return new WaitForSeconds(idleSound.length + delay);
        }
    }

    private AudioClip CreateSound(AudioClip clip)
    {
        GameObject soundOb = new GameObject("Sound_" + clip.name);
        soundOb.transform.position = transform.position;
        AudioSource _audioSource = soundOb.AddComponent<AudioSource>();
        _audioSource.pitch = Random.Range(pitchRange.x, pitchRange.y);
        _audioSource.PlayOneShot(clip);
        _audioSource.spatialBlend = 1f;
        Destroy(soundOb, clip.length);
        return clip;
    }
}
