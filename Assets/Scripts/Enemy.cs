﻿using System;
using System.Collections;
using System.Collections.Generic;
using Configs;
using UnityEngine;
using Random = UnityEngine.Random;

public class Enemy : MonoBehaviour, IDamagable
{
    private EnemyConfig _enemyConfig;
    [SerializeField] private CollideAttack _collideAttack;
    [SerializeField] private SpriteRenderer _sprite;
    private Health _health;
    private Rigidbody _rigidbody;
    private Transform _transform;
    private Transform _target;
    public Action OnKilled;
    public Action OnTookDamage;
    public bool IsAlive { private set; get; }

    public void UpdateConfig(EnemyConfig enemyConfig)
    {
        _enemyConfig = enemyConfig;
        _health = new Health(_enemyConfig.StartHp);
        _health.OnTookDamage += TookDamage;
        _health.OnHealthDepleted += HealthDepleted;
        _health.OnHpRestored += HealthRestored;
        _rigidbody = GetComponent<Rigidbody>();
        _transform = transform;
        _target = GameManager.Instance.PlayerTransform;
        _collideAttack.Init(_enemyConfig.CollideDamageCooldown, _enemyConfig.CollideDamageAmount);
        _sprite.sprite = _enemyConfig.Sprite;
        _sprite.color = _enemyConfig.SpriteColor;
        IsAlive = true;
    }
    
    private void OnDestroy()
    {
        _health.OnTookDamage -= TookDamage;
        _health.OnHealthDepleted -= HealthDepleted;
        _health.OnHpRestored -= HealthRestored;
    }

    private void Update()
    {
        _rigidbody.velocity = transform.forward * _enemyConfig.Speed;
        
        if (_enemyConfig.FacePlayer) FacePlayer();
    }

    private void FacePlayer()
    {
        Vector3 dir = _target.position - _transform.position;
        _transform.rotation = Quaternion.LookRotation(dir, Vector3.up);
        // _transform.rotation = Quaternion.LookRotation(_transform.forward, dir);
    }

    private void TookDamage(float amount)
    {
        OnTookDamage();
    }

    private void HealthDepleted()
    {
        SpawnDrop();
        OnKilled();
        Instantiate(_enemyConfig.ExplodeFx, _transform.position, _transform.rotation);
        IsAlive = false;
        Destroy(gameObject);
    }

    private void HealthRestored(float amount)
    {
        
    }

    public void Damage(float amount)
    {
        _health.TakeDamage(amount);
    }

    private void SpawnDrop()
    {
        int rndNum = Random.Range(0, 2);
        if (rndNum == 0)
        {
            if (_enemyConfig.Drop.Length <= 0) return;
            Debug.Log("Spawn drop!");
            Transform selectedDrop = _enemyConfig.Drop[Random.Range(0, _enemyConfig.Drop.Length)];
            Instantiate(selectedDrop, _transform.position, _transform.rotation);
        }
    }
}
