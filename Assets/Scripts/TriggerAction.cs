﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerAction : MonoBehaviour
{
    [SerializeField] private UnityEvent action;
    [SerializeField] private string actorTag = "Player";
    private bool _actionCalled;

    private void OnTriggerEnter(Collider other)
    {
        if (_actionCalled) return;
        if (other.CompareTag(actorTag))
        {
            action.Invoke();
            _actionCalled = true;
        }
        
    }
}
