﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KingFinish : MonoBehaviour
{
    [SerializeField] private float distanceToStartDialog = 1f;
    private Transform _playerTransform;
    private Transform _transform;
    private bool isDialogStarted;

    void Start()
    {
        _transform = transform;
        _playerTransform = GameManager.Instance.PlayerTransform;
    }

    private void Update()
    {
        if (isDialogStarted) return;
        if (Vector3.Distance(_transform.position, _playerTransform.position) < distanceToStartDialog)
        {
            isDialogStarted = true;
            DialogBox.Instance.ShowDialogArray(new[]
            {
                "Well done!",
                "This dungeon is cleared from nasty enemies!",
                "But...",
                "There is a problem...",
                "You see...",
                "Emm...",
                "Your reward is in another dungeon...",
                "Sorry!"
            }, DialogBox.SpeakerType.King, endCallback: () => { GameManager.Instance.RestartGame(); });
        }
    }
}
