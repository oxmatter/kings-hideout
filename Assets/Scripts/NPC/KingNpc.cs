﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class KingNpc : MonoBehaviour
{
    [SerializeField] private float distanceToStartDialog = 1f;
    [SerializeField] private UnityEvent speechFinishEvent;
    [SerializeField] private GameObject startDoor;

    private Transform _playerTransform;
    private Transform _transform;
    private bool isDialogStarted;
    // Start is called before the first frame update
    void Start()
    {
        _transform = transform;
        _playerTransform = GameManager.Instance.PlayerTransform;
    }

    private void Update()
    {
        if (isDialogStarted) return;
        if (Vector3.Distance(_transform.position, _playerTransform.position) < distanceToStartDialog)
        {
            isDialogStarted = true;
            DialogBox.Instance.ShowDialogArray(new[]
            {
                "Hello there! You are finally awake.",
                "Our kingdom is under attack!",
                "And I think enemies managed to sneak in this secret dungeon too.",
                "And what am I doing here?",
                "Between me and you, I'm hiding here.",
                "It doesn't mean that I'm a coward!",
                "No no no, I'm planning genius plan here!",
                "And you part of it too.",
                "Let me think...",
                "Oh, yes!",
                "You will save kingdom, and I will reward you!",
                "Here, take this! I don't know how to use this, but you might.",
                "Go on, save the Kingdom!"
            }, DialogBox.SpeakerType.King, endCallback: speechFinishEvent.Invoke);
        }
    }
}
