﻿using System;
using System.Collections;
using System.Collections.Generic;
using Configs;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private WaveConfig[] waveConfigs;
    [SerializeField] private Transform spawnPointsHolder;
    [SerializeField] private Transform enemiesHolder;
    public Action<int> OnNextWave = delegate { };
    public Action OnWaveCompleted = delegate {  };
    public UnityEvent OnCompletedAllWaves;
    private List<Transform> spawnPoints = new List<Transform>();
    private int wave;
    private bool spawningStarted;
    private WaveConfig currentWave;
    private int enemiesSpawned;
    private int enemiesKilled;
    private bool bossSpawned;

    private void Start()
    {
        foreach (Transform spawnPoint in spawnPointsHolder)
        {
            spawnPoints.Add(spawnPoint);
        } 
        GameManager.Instance.OnGameOver += () =>
        {
            spawningStarted = false;
        };
    }

    public void StartSpawning()
    {
        if (spawningStarted) return;
        NextWave();
        StartCoroutine(SpawningRoutine());
    }

    private void NextWave()
    {
        enemiesSpawned = 0;
        enemiesKilled = 0;
        bossSpawned = false;
        wave++;
        Debug.Log($"Next wave {wave}");
        if (wave > waveConfigs.Length)
        {
            spawningStarted = false;
            OnCompletedAllWaves.Invoke();
        }
        else
        {
            currentWave = waveConfigs[wave - 1];
            OnNextWave(wave);
        }
    }

    IEnumerator SpawningRoutine()
    {
        yield return new WaitForSeconds(2);
        spawningStarted = true;
        while (spawningStarted)
        {
            if (enemiesSpawned >= currentWave.SpawnAmount)
            {
                if (enemiesKilled >= currentWave.SpawnAmount)
                {
                    OnWaveCompleted();
                    yield return new WaitForSeconds(currentWave.NextWaveWaitSeconds);
                    NextWave();
                }

                yield return null;
            }
            else
            {
                if (!bossSpawned && currentWave.Boss != null)
                {
                    SpawnBoss();
                }
                else
                {
                    SpawnEnemy();
                }

                yield return new WaitForSeconds(currentWave.EnemySpawnDelay);
            }
        }
    }

    private void SpawnBoss()
    {
        bossSpawned = true;
        enemiesSpawned++;
        Transform selectedSpawnPoint = GetRandomSpawnPoint();
        Transform spawnedBossTransform =
            Instantiate(currentWave.Boss.EnemyTransform, selectedSpawnPoint.position, selectedSpawnPoint.rotation);
        spawnedBossTransform.parent = enemiesHolder;
        Enemy spawnedEnemy = spawnedBossTransform.GetComponent<Enemy>();
        spawnedEnemy.UpdateConfig(currentWave.Boss);
        spawnedEnemy.OnKilled += () => { enemiesKilled++; };
    }

    private void SpawnEnemy()
    {
        enemiesSpawned++;
        Transform selectedSpawnPoint = GetRandomSpawnPoint();
        EnemyConfig SelectedConfig = GetRandomEnemyConfig();
        Transform spawnedEnemyTransform =
            Instantiate(SelectedConfig.EnemyTransform, selectedSpawnPoint.position, selectedSpawnPoint.rotation);
        spawnedEnemyTransform.parent = enemiesHolder;
        Enemy spawnedEnemy = spawnedEnemyTransform.GetComponent<Enemy>();
        spawnedEnemy.UpdateConfig(SelectedConfig);
        spawnedEnemy.OnKilled += () => { enemiesKilled++; };

    }

    private EnemyConfig GetRandomEnemyConfig()
    {
        int rndIndex = Random.Range(0, currentWave.EnemyConfigs.Length);
        return currentWave.EnemyConfigs[rndIndex];
    }

    private Transform GetRandomSpawnPoint()
    {
        return spawnPoints[Random.Range(0, spawnPoints.Count)];
    }
}
