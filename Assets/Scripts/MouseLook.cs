﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    [SerializeField] private float _sensetivity = 1;
    [SerializeField] private float smoothness = 1;
    [SerializeField] private float clamp = 90;
    private Transform _playerBody;
    private Transform _cameraHolder;
    private Quaternion playerBodyTargetRot;
    private Quaternion camTargetRot;
    private bool cursorLocked;

    public void Init(Transform playerBody, Transform cameraHolder)
    {
        _playerBody = playerBody;
        playerBodyTargetRot = playerBody.localRotation;
        _cameraHolder = cameraHolder;
        camTargetRot = cameraHolder.localRotation;
    }

    private void Awake()
    {
        cursorLocked = true;
    }

    public void MoveLook(float inputX, float inputY)
    {
        UpdateCursorVisibility();
        if (!cursorLocked) return;
        float yRot = inputX * _sensetivity;
        float xRot = inputY * _sensetivity;

        playerBodyTargetRot *= Quaternion.Euler(0, yRot, 0);
        camTargetRot *= Quaternion.Euler(-xRot,0,0);

        camTargetRot = ClampQuaternionX(camTargetRot);
        
        _playerBody.localRotation = Quaternion.Slerp(_playerBody.localRotation, playerBodyTargetRot, smoothness * Time.deltaTime);
        _cameraHolder.localRotation =
            Quaternion.Slerp(_cameraHolder.localRotation, camTargetRot, smoothness * Time.deltaTime);
    }

    private void UpdateCursorVisibility()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            cursorLocked = false;
        }else if (Input.GetMouseButtonUp(0))
        {
            cursorLocked = true;
        }

        LockCursor(cursorLocked);
    }

    public void LockCursor(bool locked)
    {
        if (locked)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    private Quaternion ClampQuaternionX(Quaternion q)
    {
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1;

        float angle = 2f * Mathf.Rad2Deg * Mathf.Atan(q.x);

        angle = Mathf.Clamp(angle, -clamp, clamp);

        q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angle);
        return q;
    }
}
