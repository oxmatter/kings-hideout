﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteTexture : MonoBehaviour
{
    [SerializeField] private Sprite _sprite;
    [SerializeField] private MeshRenderer _meshRenderer;

    private void Awake()
    {
        Texture2D texture = new Texture2D(Mathf.CeilToInt(_sprite.rect.width), Mathf.CeilToInt(_sprite.rect.height));
        Color[] pixels = _sprite.texture.GetPixels(
            Mathf.CeilToInt(_sprite.textureRect.x),
            Mathf.CeilToInt(_sprite.textureRect.y),
            Mathf.CeilToInt(_sprite.textureRect.width),
            Mathf.CeilToInt(_sprite.textureRect.height));
        texture.SetPixels(pixels);
        texture.filterMode = FilterMode.Point;
        texture.Apply();
        _meshRenderer.material.mainTexture = texture;
    }
}
