﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceCamera : MonoBehaviour
{
    private Transform _camTr;
    private Transform _transform;
    private void Start()
    {
        _camTr = GameManager.Instance.CameraTransform;
        _transform = transform;
    }

    private void LateUpdate()
    {
        var camTrRotation = _camTr.rotation;
        Vector3 targetPos = _transform.position + camTrRotation * Vector3.forward;
        Vector3 targetOrientation = camTrRotation * Vector3.up;
        transform.LookAt(new Vector3(targetPos.x, _transform.position.y, targetPos.z), targetOrientation);
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
    }
}
