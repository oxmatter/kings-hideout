﻿using UnityEngine;

namespace Configs
{
    [CreateAssetMenu(fileName = "WaveConfig", menuName = "configs/WaveConfig", order = 0)]
    public class WaveConfig : ScriptableObject
    {
        [SerializeField] private EnemyConfig[] _enemyConfigs;
        public EnemyConfig[] EnemyConfigs => _enemyConfigs;
        [SerializeField] private EnemyConfig _boss;
        public EnemyConfig Boss => _boss;
        
        [SerializeField] private int _spawnAmount = 1;
        public int SpawnAmount => _spawnAmount;
        
        [SerializeField] private float _enemySpawnDelay = 2f;
        public float EnemySpawnDelay => _enemySpawnDelay;
        
        [SerializeField] private float _nextWaveWaitSeconds = 10f;
        public float NextWaveWaitSeconds => _nextWaveWaitSeconds;
    }
}