﻿using UnityEngine;

namespace Configs
{
    [CreateAssetMenu(fileName = "EnemyConfig", menuName = "configs/EnemyConfig", order = 0)]
    public class EnemyConfig : ScriptableObject
    {
        [SerializeField]
        private Transform _enemyTransform;
        public Transform EnemyTransform => _enemyTransform;
        
        [SerializeField] private float _startHp;
        public float StartHp => _startHp;
        
        [SerializeField] private float _speed = 2;
        public float Speed => _speed;

        [SerializeField] private Sprite _sprite;
        public Sprite Sprite => _sprite;
        [SerializeField] private Color _spriteColor;
        public Color SpriteColor => _spriteColor;

        [SerializeField] private bool _facePlayer;
        public bool FacePlayer => _facePlayer;
        [SerializeField] private float _collideDamageCooldown = 1f;

        public float CollideDamageCooldown => _collideDamageCooldown;
        [SerializeField] private float _collideDamageAmount = 10f;

        public float CollideDamageAmount => _collideDamageAmount;
        [SerializeField] private Transform[] _drop;
        public Transform[] Drop => _drop;
        [SerializeField] private Transform explodeFx;

        public Transform ExplodeFx => explodeFx;
    }
}